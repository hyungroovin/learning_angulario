#About the program

This program was done from Deborah K's learning Angular tutorial on plural sight to learn the differences between AngularJs and AngularIo
key learning concepts displayed are:setting up the application modules with their respective components, understanding how decorators add meta data concerning component, understanding how to properly export/import components, setting up a logical structure for the components (css files, html view files, model class files, etc), understanding angular.io's custom directives, understanding how angular.io does piping, understanding how angular.io does data binding, understanding angular.io's hooks for component life cycles, setting up and using filters, using nested components(event binding)

# APM

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

