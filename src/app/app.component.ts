
import {Component} from '@angular/core';

//pm means product management. Current convention is to prefix it with something that identifies the application. End with name that represents component. 
@Component({
  selector: 'pm-root',
  template: `
  <nav class = 'navbar navbar-expand navbar-light bg-light'>
  <a class='navbar-brand'>{{pageTitle}}</a>
  <ul class='nav nav-pills'>
    <li><a class='nav-link' [routerLink]="['/welcome']">Home</a></li>
    <li><a class='nav-link' [routerLink]="['/products']">Product List</a></li>
    </ul>
    </nav>
    <div class='container'>
    <router-outlet></router-outlet>
    </div>
  `
})

export class AppComponent{
  pageTitle: string = "Trying out Angular.io";
}